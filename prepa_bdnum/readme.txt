La préparation des données alpha-numériques suit trois grandes étapes :

    step1 : de la BD brute à la BD clean : réception, nettoyage et prétraitement des données brutes
    step2 : de la BD clean à la BD presence : transformation des déplacements en présences
    step3 : de la BD presence aux indicateurs du Mobiliscope : agrégation des présences par heure, secteur et modalités 
