Les données brutes ont été traitées par zone géographique :

ameriquelatine : prétraitements des données d'enquête ménage-déplacement de Bogota, Sao Paulo et Santiago réalisés pour le Mobiliscope v4.1

canada : prétraitements des données d'enquête origine-destination de Montréal, Québec, Ottawa-Gatineau, Saguenay, Sherbrooke et Trois-Rivières révisés pour le Mobiliscope v4.1

france : 
 cerema : prétraitements des données d'enquête ménage-déplacement (Base Unifiée du Cerema) de 49 villes et leur territoire révisés pour le Mobiliscope v4.1
 egt2020 : prétraitements des données de l'enquête globale transport d'Ile-de-France (EGT 2018-2020 fournie par Progedo) réalisés au printemps 2023 - enquête non intégrée au Mobiliscope


Après traitement, les tables "déplacement" et "personne" des différentes zones géographiques sont compilées pour obtenir une base harmonisée : BD_personne_clean.RDS et BD_deplacement_clean.RDS (stockées hors git)