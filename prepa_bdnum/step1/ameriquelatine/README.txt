Ce dossier contient les prétraitements effectués sur les données brutes des enquêtes latino-américaines.

- bdBrute2bdm.R: construction et nettoyage des tables déplacements et personnes latino-américaines.
en entrée : BD des enquêtes américaines
en sortie : BD_deplacement_as.RDS et BD_personne_as.RDS

- dossier "txt" : contient les fichiers csv créés et utilisés par les script R

Les données des EOD ont été téléchargées au début de l’année 2021 aux adresses suivantes :
-	Bogotá : https://www.simur.gov.co/encuestas-de-movilidad
-	Santiago : http://www.sectra.gob.cl/biblioteca/detalle1.asp?mfn=3253
-	São Paulo : http://www.metro.sp.gov.br/pesquisa-od/