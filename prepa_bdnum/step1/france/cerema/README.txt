Ce dossier contient les ressources utilisés pour le prétraitement des enquêtes françaises (hors egt2020)

1_zonage_residentialArea.R : construction du zonage (ZAU simplifié) d'appartenance des secteurs

2_bdu2bdm.R : création d'une BD standardisée avec une table déplacement et une table personne à partir de la base unifiée du cerema
	
3_qpv : construction de l'indicateur "résidence en/hors QPV" 

En entrée : BDU du cerema
En sortie : BD_deplacement_fr.RDS et BD_personne_fr.RDS


NB : Les données françaises ne sont pas en accès libre sauf pour Lille, Nantes et Montpellier (https://mobiliscope.cnrs.fr/fr/info/methods/data)


