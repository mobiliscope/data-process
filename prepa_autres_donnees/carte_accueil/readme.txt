Contenu des dossiers :

- franceCanada : construction des couches de la carte d'accueil pour la version 4.0 du Mobiliscope
- ameriqueLatine : construction des couches de la carte d'accueil "Amérique latine" et reprise des centroïdes de la carte "France" - pour la version 4.1 du Mobiliscope