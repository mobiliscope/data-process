#==============================================================================#
#              Construction du fichier codenamesec.json v4-3
#               
# description : liste des secteurs, code et libellé au format json 
#               Fichier complémentaire à l'outil search
# 
# AD, mars 2023
#==============================================================================#

# librariries
library(tidyverse)
library(jsonlite)

# load table des libellés
lib <- read_csv2("../../../../00_BD_mobiliscope/ressources/LIB_SEC_59ED_V4-3.csv")

# mise en forme
lib <- lib %>% 
  mutate(id = reduce2(.x = " ", .y = "-", .init = tolower(ENQUETE), str_replace),
         codesec = word(ID_SEC, 3, 3, "_"),
         libsec = case_when(!is.na(LIB_ACC) ~ LIB_ACC,
                            TRUE ~ LIB_MAJ)) %>% 
  select(id, codesec, libsec)


sort(unique(lib$id))

# save in www
#!!!!! attention à être branché au bon endroit !!!!!

# Branche minio :
write_json(lib, "C:/wamp64/www/mobiliscope/data-settings/codenamesec.json")
