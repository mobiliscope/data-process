# data-process of Mobiliscope

This repository is dedicated to the R preprocessing of the Mobiliscope data.

[Mobiliscope](https://mobiliscope.cnrs.fr/) is a free and open-source web mapping platform for the interactive exploration of cities around the clock. The Mobiliscope source code is available [here](https://gitlab.huma-num.fr/mobiliscope/www).

More information in the Guide Administrateur·rice (v4.3) : https://mobiliscope.quarto.pub/guide_admin/guide_admin.html


## Folders and files available in the repository

### prepa_bdnum
R code to create/update alpha-numeric databases

### prepa_bdgeo
R code used to update the geographical database

### prepa_autres_donnees
R code to update other data displayed on the website

### guide_admin
Contains the administrator's guide in French, a reference document for maintaining and developing the Mobiliscope project.

### traitements_annexes
Folder containing other related treatments

## License
The R code of Mobiliscope data-process is licensed under the **AGPL-3.0 License**. You can use and modify programs as long as they redistribute the modified programs under AGPL license and indicated sources, for example as follow: « Programs under AGPL license issued from Mobiliscope ». 
Please have a look at the terms and conditions (https://www.gnu.org/licenses/agpl-3.0.fr.html) for copying, distribution and modification.

## Contact
mobiliscope[at]cnrs.fr
